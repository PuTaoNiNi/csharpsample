﻿using System;
using System.Diagnostics;

namespace BoxingAndUnboxing
{
    class Program
    {
        static void Main(string[] args)
        {
            var stopwatch = new Stopwatch();
            var count = 100_000_000;


            object obj = 123;

            Stopwatch(stopwatch, count, obj, WithoutBox);
            Stopwatch(stopwatch, count, obj, Boxing);
            Stopwatch(stopwatch, count, obj, UnBoxing);
        }

        private static void Stopwatch(Stopwatch stopwatch, int count, object obj, Action<object> action)
        {
            stopwatch.Restart();

            for (int i = 0; i < count; i++)
            {
                action(obj);
            }

            stopwatch.Stop();

            Console.WriteLine($"{action.Method.Name} Test 耗時 {stopwatch.ElapsedMilliseconds.ToString()} 毫秒");
        }

        private static void WithoutBox(object obj)
        {
            int i = 123;
        }

        private static void Boxing(object obj)
        {
            object i = 123;
        }

        private static void UnBoxing(object obj)
        {
            int i = (int)obj;
        }
    }
}
