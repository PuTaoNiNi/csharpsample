﻿// See https://aka.ms/new-console-template for more information

var solution = new Solution();

//var costs1 = new int[][] { new int[] { 10, 20 }, new int[] { 30, 200 }, new int[] { 400, 50 }, new int[] { 30, 20 } };
//var output1 = solution.TwoCitySchedCost(costs1);
//Console.WriteLine(output1); // 110

//var costs2 = new int[][] { new int[] { 259, 770 }, new int[] { 448, 54 }, new int[] { 926, 667 }, new int[] { 184, 139 }, new int[] { 840, 118 }, new int[] { 577, 469 } };
//var output2 = solution.TwoCitySchedCost(costs2);
//Console.WriteLine(output2); // 1859

//var costs3 = new int[][] { new int[] { 515, 563 }, new int[] { 451, 713 }, new int[] { 537, 709 }, new int[] { 343, 819 }, new int[] { 855, 779 }, new int[] { 457, 60 }, new int[] { 650, 359 }, new int[] { 631, 42 } };
//var output3 = solution.TwoCitySchedCost(costs3);
//Console.WriteLine(output3); // 3086

//var costs4 = new int[][] { new int[] { 2, 4 }, new int[] { 1, 2 }, new int[] { 3, 6 } };
//var output4 = solution.TwoCitySchedCost(costs4);
//Console.WriteLine(output4);

var costs5 = new int[][] { new int[] { 3, 1 }, new int[] { 1, 4 }, new int[] { 2, 1 }, new int[] { 1, 1 } };
var output5 = solution.TwoCitySchedCost(costs5);
Console.WriteLine(output5);

public class Solution
{
    public int TwoCitySchedCost(int[][] costs)
    {
        //Array.Sort(costs, (a, b) =>
        //{
        //    Console.WriteLine($"{a[0]} - {a[1]} = {a[0] - a[1]}");
        //    Console.WriteLine($"{b[0]} - {b[1]} = {b[0] - b[1]}");
        //    Console.WriteLine($"{a[0] - a[1] - (b[0] - b[1])}");
        //    return a[0] - a[1] - (b[0] - b[1]);
        //});

        Array.Sort(costs, (a, b) =>
        {
            Console.WriteLine($"{a[1]} - {a[0]} = {a[1] - a[0]}");
            Console.WriteLine($"{b[1]} - {b[0]} = {b[1] - b[0]}");
            Console.WriteLine($"{(b[1] - b[0]) - (a[1] - a[0])}");
            return (b[1] - b[0]) - (a[1] - a[0]);
        });

        //Array.Sort(costs, (a, b) =>
        //{
        //    Console.WriteLine($"{a[0] - b[0]}");
        //    return a[0] - b[0];
        //});

        var diffCosts = new int[costs.Length];
        var result = 0;
        for (int i = 0; i < costs.Length; i++)
        {
            diffCosts[i] = costs[i][1] - costs[i][0];
            result += costs[i][0];
        }

        Array.Sort(diffCosts);

        for (int i = 0; i < costs.Length / 2; i++)
        {
            result += diffCosts[i];
        }

        return result;
    }
}

public class Solution1
{
    public int TwoCitySchedCost(int[][] costs)
    {
        var sum = 0;
        var n = costs.Length / 2;
        Array.Sort(costs, (a, b) => (b[1] - b[0]) - (a[1] - a[0]));
        Array.Sort(costs, (a, b) => a[0] - a[1] - (b[0] - b[1]));
        for (var i = 0; i < costs.Length / 2; ++i)
        {
            sum += costs[i][0] + costs[n + i][1];
        }

        return sum;
    }
}