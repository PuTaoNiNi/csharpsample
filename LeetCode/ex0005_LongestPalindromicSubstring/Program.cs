﻿var solution = new Solution();

var s1 = "babad";
var output1 = solution.LongestPalindrome(s1);
Console.WriteLine(output1);

var s2 = "cbbd";
var output2 = solution.LongestPalindrome(s2);
Console.WriteLine(output2);


public class Solution
{
    public string LongestPalindrome(string s)
    {
        var n = s.Length;
        string res = null;
        int start = 0, length = 0;
        var dp = new bool[n, n];

        for (int i = n - 1; i >= 0; i--)
        {
            for (int j = i; j < n; j++)
            {
                dp[i, j] = s[i] == s[j] && (j - i < 3 || dp[i + 1, j - 1]);

                if (dp[i, j] && j - i + 1 > length)
                {
                    start = i;
                    length = (j - i) + 1;
                }
            }
        }

        return n == 0 ? "" : s.Substring(start, length);
    }
}
