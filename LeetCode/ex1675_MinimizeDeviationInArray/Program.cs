﻿// See https://aka.ms/new-console-template for more information
var solution = new Solution();

//var input1 = new int[] { 1, 2, 3, 4 };
//var output1 = solution.MinimumDeviation(input1);
//Console.WriteLine(output1);

//var input2 = new int[] { 4, 1, 5, 20, 3 };
//var output2 = solution.MinimumDeviation(input2);
//Console.WriteLine(output2);

//var input3 = new int[] { 2, 10, 8 };
//var output3 = solution.MinimumDeviation(input3);
//Console.WriteLine(output3);

var input4 = new int[] { 3, 5 };
var output4 = solution.MinimumDeviation(input4);
Console.WriteLine(output4);



public class Solution
{
    public int MinimumDeviation(int[] nums)
    {
        for (int i = 0; i < nums.Length; i++)
        {
            if (nums[i] % 2 == 1)
                nums[i] = nums[i] * 2;
        }


        Array.Sort(nums);
        var result = nums.Last() - nums.First();

        while (nums.Last() % 2 == 0)
        {
            nums[nums.Length - 1] = nums.Last() / 2;
            Array.Sort(nums);
            result = Math.Min(result, nums.Last() - nums.First());
        }

        return result;
    }
}

// priority_queue
public class Solution2
{
    public int MinimumDeviation(int[] nums)
    {

        SortedSet<int> set = new SortedSet<int>();

        for (int i = 0; i < nums.Length; i++)
        {

            if (nums[i] % 2 == 0)
            {
                set.Add(nums[i]);
            }
            else
            {
                set.Add(nums[i] * 2);
            }

        }
        var min = set.Max - set.Min;
        while (set.Max % 2 == 0)
        {
            var maks = set.Max;
            set.Remove(maks);
            set.Add(maks / 2);
            min = Math.Min(min, set.Max - set.Min);


        }
        return min;
    }
}

public class Solutionˇ3
{
    public int CompareVersion(string version1, string version2)
    {
        var l1 = version1.Split('.');
        var l2 = version2.Split('.');

        int i1, i2;
        for (int i = 0; i < Math.Max(l1.Length, l2.Length); i++)
        {
            i1 = i < l1.Length ? int.Parse(l1[i]) : 0;
            i2 = i < l2.Length ? int.Parse(l2[i]) : 0;

            if (i1 != i2)
            {
                return i1 > i2 ? 1 : -1;
            }
        }
        return 0;
    }
}