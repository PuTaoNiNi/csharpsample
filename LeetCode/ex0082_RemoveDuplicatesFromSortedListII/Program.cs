﻿// See https://aka.ms/new-console-template for more information
using ModelLibrary;

var solution = new Solution();

ListNode head1 = new(1, new(2, new(3, new(3, new(4, new(4, new(5)))))));
var output1 = solution.DeleteDuplicates(head1);
Console.WriteLine(output1.ToString());

ListNode head2 = new(1, new(1, new(1, new(2, new(3)))));
var output2 = solution.DeleteDuplicates(head2);
Console.WriteLine(output2.ToString());

ListNode head3 = null;
var output3 = solution.DeleteDuplicates(head3);
Console.WriteLine(output3);

public class Solution
{
    public ListNode DeleteDuplicates(ListNode head)
    {
        var result = new ListNode(0, head);
        var pre = result;

        while (head != null && head.next != null)
        {
            if (head.val == head.next.val)
            {
                head = head.next;

                while (head.next != null && head.val == head.next.val)
                {
                    head = head.next;
                }

                pre.next = head.next;
            }
            else
            {
                pre = head;
            }

            head = head.next;
        }

        return result.next;
    }
}