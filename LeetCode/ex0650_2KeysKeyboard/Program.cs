﻿// See https://aka.ms/new-console-template for more information

var solution = new Solution();

var n1 = 3;
var output1 = solution.MinSteps(n1);
Console.WriteLine(output1);

var n2 = 1;
var output2 = solution.MinSteps(n2);
Console.WriteLine(output2);

var n3 = 6;
var output3 = solution.MinSteps(n3);
Console.WriteLine(output3);

var n4 = 7;
var output4 = solution.MinSteps(n4);
Console.WriteLine(output4);


public class Solution
{
    public int MinSteps(int n)
    {
        var result = 0;
        var d = 2;
        while (n > 1)
        {
            while (n % d == 0)
            {
                result += d;
                n /= d;
            }
            d++;
        }

        return result;
    }
}