﻿var solution = new Solution1();

var s1 = "abccbabb";
var output1 = solution.LengthOfLongestSubstring(s1);
Console.WriteLine(output1);

var s2 = "bbbbb";
var output2 = solution.LengthOfLongestSubstring(s2);
Console.WriteLine(output2);

var s3 = "pwwkew";
var output3 = solution.LengthOfLongestSubstring(s3);
Console.WriteLine(output3);

var s4 = " ";
var output4 = solution.LengthOfLongestSubstring(s4);
Console.WriteLine(output4);


public class Solution
{
    public int LengthOfLongestSubstring(string s)
    {
        if (s.Length == 0) { return 0; }

        var result = 0;
        var hashSet = new HashSet<char>();
        var repeatIndex = 0;
        var i = 0;
        while (i < s.Length)
        {
            if (!hashSet.Contains(s[i]))
            {
                hashSet.Add(s[i]);
                i++;
                result = Math.Max(result, i - repeatIndex);
            }
            else
            {
                hashSet.Remove(s[repeatIndex]);
                repeatIndex++;
            }
        }

        return result;
    }
}

public class Solution1
{
    public int LengthOfLongestSubstring(string s)
    {
        var result = 0;
        var dic = new Dictionary<char, int>();

        for (int i = 0, j = 0; i < s.Length; i++)
        {
            if (dic.ContainsKey(s[i]))
            {
                j = Math.Max(j, dic[s[i]]);
            }

            result = Math.Max(result, i - j + 1);
            dic[s[i]] = i + 1;
        }

        return result;
    }
}