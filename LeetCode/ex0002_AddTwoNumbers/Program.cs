﻿using ModelLibrary;

var solution = new Solution();
ListNode input1_l1 = new(2, new(4, new(3)));
ListNode input1_l2 = new(5, new(6, new(4)));
var output1 = solution.AddTwoNumbers(input1_l1, input1_l2);
Console.WriteLine(output1.ToString());

ListNode input2_l1 = new(9, new(9, new(9)));
ListNode input2_l2 = new(9, new(9));
var output2 = solution.AddTwoNumbers(input2_l1, input2_l2);
Console.WriteLine(output2.ToString());

public class Solution
{
    public ListNode AddTwoNumbers(ListNode l1, ListNode l2)
    {
        ListNode head = new();
        var current = head;

        var carry = 0;

        while (l1 != null || l2 != null)
        {
            var val1 = (l1 == null) ? 0 : l1.val;
            var val2 = (l2 == null) ? 0 : l2.val;

            var sum = val1 + val2 + carry;

            current.val = sum % 10;
            carry = sum / 10;

            if (l1 != null) l1 = l1.next;
            if (l2 != null) l2 = l2.next;

            if (l1 != null || l2 != null)
            {
                current.next = new();
                current = current.next;
            }
        }

        if (carry > 0)
        {
            current.next = new(1);
        }

        return head;
    }
}

public class Solution1
{
    public ListNode AddTwoNumbers(ListNode l1, ListNode l2)
    {
        ListNode head = new(0);
        var current = head;

        var carry = 0;

        while (l1 != null || l2 != null)
        {
            var val1 = (l1 == null) ? 0 : l1.val;
            var val2 = (l2 == null) ? 0 : l2.val;

            var sum = val1 + val2 + carry;

            current.next = new(sum % 10);
            current = current.next;

            carry = sum / 10;

            if (l1 != null) l1 = l1.next;
            if (l2 != null) l2 = l2.next;
        }

        if (carry > 0)
        {
            current.next = new(1);
        }

        return head.next;
    }
}
