﻿var solution = new Solution();

//var input1 = new int[] { 1, 2, 3, 3, 3, 3, 3, 4 };
//var output1 = solution.RemoveDuplicates(input1);
//Console.WriteLine(output1.ToString());

//var input2 = new int[] { };
//var output2 = solution.RemoveDuplicates(input2);
//Console.WriteLine(output2.ToString());


var input3 = new int[] { 1, 1, 2 };
var output3 = solution.RemoveDuplicates(input3);
Console.WriteLine(output3.ToString());


public class Solution
{
    public int RemoveDuplicates(int[] nums)
    {
        if (nums.Length == 0)
            return 0;

        var count = 0;

        for (int i = 1; i < nums.Length; i++)
        {
            if (nums[i] != nums[count])
            {
                count++;
                nums[count] = nums[i];
            }
        }

        return count + 1;
    }
}


public class Solution1
{
    public int RemoveDuplicates(int[] nums)
    {
        var count = 0;
        for (int i = 1; i < nums.Length; i++)
        {
            if (nums[i] == nums[i - 1])
            {
                count++;
            }
            else
            {
                nums[i - count] = nums[i];
            }
        }

        return nums.Length - count;
    }
}