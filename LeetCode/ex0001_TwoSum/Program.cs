﻿using System.Text.Json;

var solution = new Solution();
var nums = new int[] { 2, 7, 11, 15 };
var target = 9;
var result = solution.TwoSum(nums, target);

Console.WriteLine(JsonSerializer.Serialize(result));

public class Solution
{
    /* 概念 target - current value = 差值 
     * 透過 Dictionary 紀錄 Key:差值 跟 Value:index
     */

    public int[] TwoSum(int[] nums, int target)
    {
        var dic = new Dictionary<int, int>();

        for (int i = 0; i < nums.Length; i++)
        {
            if (dic.ContainsKey(target - nums[i]))
                return new int[] { dic[target - nums[i]], i };

            if (!dic.ContainsKey(nums[i]))
                dic.Add(nums[i], i);
        }

        return new int[2];
    }
}