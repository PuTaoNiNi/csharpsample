﻿// See https://aka.ms/new-console-template for more information

var solution = new Solution1();

var input1 = 2;
var output1 = solution.CountBits(input1);
Console.WriteLine($"[{string.Join(", ", output1)}]");

var input2 = 5;
var output2 = solution.CountBits(input2);
Console.WriteLine($"[{string.Join(", ", output2)}]");

public class Solution
{
    public int[] CountBits(int n)
    {
        if (n == 0)
        {
            return new int[] { 0 };
        }

        var result = new int[n + 1];
        result[0] = 0;

        for (int i = 1; i <= n; i++)
        {
            result[i] = result[i >> 1] + (i & 1);
        }

        return result;
    }
}

public class Solution1
{
    public int[] CountBits(int n)
    {
        if (n == 0)
        {
            return new int[] { 0 };
        }

        var result = new int[n + 1];
        result[0] = 0;

        for (int i = 1; i <= n; i++)
        {
            result[i] = result[i / 2] + (i % 2);
        }

        return result;
    }
}

