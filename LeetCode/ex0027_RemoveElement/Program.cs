﻿// See https://aka.ms/new-console-template for more information
var solution = new Solution1();

//var input1_nums = new int[] { 3, 2, 3, 3 };
//var input1_val = 3;
//var output1 = solution.RemoveElement(input1_nums, input1_val);
//Console.WriteLine(output1);

//var input2_nums = new int[] { 0, 1, 2, 2, 3, 0, 4, 2 };
//var input2_val = 2;
//var output2 = solution.RemoveElement(input2_nums, input2_val);
//Console.WriteLine(output2);

//var input3_nums = new int[] { 3, 3 };
//var input3_val = 3;
//var output3 = solution.RemoveElement(input3_nums, input3_val);
//Console.WriteLine(output3);

var input4_nums = new int[] { 3, 2, 2, 3 };
var input4_val = 3;
var output4 = solution.RemoveElement(input4_nums, input4_val);
Console.WriteLine(output4);


public class Solution
{
    public int RemoveElement(int[] nums, int val)
    {
        var count = 0;
        for (int i = 0; i < nums.Length; i++)
        {
            if (nums[i] == val)
            {
                count++;
                while (i < nums.Length - 1)
                {
                    if (nums.Length - count != i && nums[nums.Length - count] == val)
                    {
                        count++;
                    }
                    else
                    {
                        break;
                    }
                }

                nums[i] = nums[nums.Length - count];
            }

            if (nums.Length - count == 0 || i + 1 >= nums.Length - count)
                break;
        }

        return nums.Length - count;
    }
}

public class Solution1
{
    public int RemoveElement(int[] nums, int val)
    {
        int length = nums.Length;
        int i = 0;
        while (i < length)
        {
            if (nums[i] == val)
            {
                for (int j = i + 1; j < length; j++)
                    nums[j - 1] = nums[j];

                length--;
            }
            else
            {
                i++;
            }
        }

        return length;
    }
}

public class Solution2
{
    public int RemoveElement(int[] nums, int val)
    {
        var count = 0;
        for (int i = 0; i < nums.Length; i++)
        {
            if (nums[i] != val)
            {
                if (i != count)
                {
                    nums[count] = nums[i];
                }

                count++;
            }
        }

        return count;
    }
}