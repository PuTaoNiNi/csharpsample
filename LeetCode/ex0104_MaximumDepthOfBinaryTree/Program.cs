﻿// See https://aka.ms/new-console-template for more information
var solution = new Solution();

var input1_nums = new TreeNode(3, new(9), new(20, new(15), new(7)));
var output1 = solution.MaxDepth(input1_nums);
Console.WriteLine(output1);

var input2_nums = new TreeNode(1, null, new(2));
var output2 = solution.MaxDepth(input2_nums);
Console.WriteLine(output2);


public class TreeNode
{
    public int val;

    public TreeNode left;

    public TreeNode right;

    public TreeNode(int val = 0, TreeNode left = null, TreeNode right = null)
    {
        this.val = val;
        this.left = left;
        this.right = right;
    }
}

public class Solution
{
    public int MaxDepth(TreeNode root)
    {
        return root == null ? 0 : Math.Max(MaxDepth(root.left), MaxDepth(root.right)) + 1;
    }
}