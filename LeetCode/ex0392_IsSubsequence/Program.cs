﻿// See https://aka.ms/new-console-template for more information

var solution = new Solution1();

var input1_1 = "abc";
var input1_2 = "ahbgdc";
var output1 = solution.IsSubsequence(input1_1, input1_2);
Console.WriteLine(output1);

var input2_1 = "axc";
var input2_2 = "ahbgdc";
var output2 = solution.IsSubsequence(input2_1, input2_2);
Console.WriteLine(output2);

var input3_1 = "acb";
var input3_2 = "ahbgdc";
var output3 = solution.IsSubsequence(input3_1, input3_2);
Console.WriteLine(output3);

var input4_1 = "aaaaaa";
var input4_2 = "bbaaaa";
var output4 = solution.IsSubsequence(input4_1, input4_2);
Console.WriteLine(output4);

public class Solution
{
    public bool IsSubsequence(string s, string t)
    {
        if (string.IsNullOrEmpty(s))
        {
            return true;
        }

        var index = t.IndexOf(s[0]);

        if (index == -1)
        {
            return false;
        }

        for (int i = 1; i < s.Length; i++)
        {
            var currentIndex = t.IndexOf(s[i], index + 1);

            Console.WriteLine(currentIndex);

            if (index > currentIndex)
            {
                return false;
            }

            index = currentIndex;
        }

        return true;
    }
}


public class Solution1
{
    public bool IsSubsequence(string s, string t)
    {
        if (s.Length == 0)
            return true;

        var i = 0;

        for (var j = 0; j < t.Length; j++)
            if (s[i] == t[j] && ++i == s.Length)
                return true;

        return false;
    }
}