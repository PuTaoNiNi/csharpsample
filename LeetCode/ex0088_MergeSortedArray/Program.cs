﻿// See https://aka.ms/new-console-template for more information

var solution = new Solution();

//int[] nums1_1 = new int[] { 1, 2, 3, 0, 0, 0 }, nums2_1 = new int[] { 2, 3, 6 };
//int m1 = 3, n1 = 3;
//solution.Merge(nums1_1, m1, nums2_1, n1);

//int[] nums1_2 = new int[] { 1 }, nums2_2 = new int[] { };
//int m2 = 1, n2 = 0;
//solution.Merge(nums1_2, m2, nums2_2, n2);

int[] nums1_3 = new int[] { 0 }, nums2_3 = new int[] { 1 };
int m3 = 0, n3 = 1;
solution.Merge(nums1_3, m3, nums2_3, n3);

int[] nums1_4 = new int[] { 2, 3, 4, 0, 0, 0 }, nums2_4 = new int[] { 1, 2, 4 };
int m4 = 3, n4 = 3;
solution.Merge(nums1_4, m4, nums2_4, n4);

public class Solution
{
    public void Merge(int[] nums1, int m, int[] nums2, int n)
    {
        int i = m - 1, j = n - 1, k = m + n - 1;

        while (i >= 0 && j >= 0)
        {
            if (nums1[i] > nums2[j])
            {
                nums1[k--] = nums1[i--];
            }
            else
            {
                nums1[k--] = nums2[j--];
            }
        }

        while (j >= 0)
        {
            nums1[k--] = nums2[j--];
        }
    }
}