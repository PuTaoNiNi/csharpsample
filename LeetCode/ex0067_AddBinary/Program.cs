﻿// See https://aka.ms/new-console-template for more information

var solution = new Solution();

string a1 = "11", b1 = "1";
Console.WriteLine(solution.AddBinary(a1, b1));

string a2 = "1010", b2 = "1011";
Console.WriteLine(solution.AddBinary(a2, b2));

string a3 = "0", b3 = "0";
Console.WriteLine(solution.AddBinary(a3, b3));

public class Solution
{
    public string AddBinary(string a, string b)
    {
        if (a == null && b == null)
        {
            return null;
        }
        else if (a == null || b == null)
        {
            return a ?? b;
        }
        else
        {
            var res = "";
            int i = a.Length - 1;
            int j = b.Length - 1;
            int carry = 0;
            while (i >= 0 || j >= 0)
            {
                int sum = carry;

                if (i >= 0) sum += a[i--] - '0';
                if (j >= 0) sum += b[j--] - '0';

                carry = sum > 1 ? 1 : 0;

                res = $"{(sum % 2).ToString()}{res}";
            }
            if (carry != 0) res = $"{(carry % 2).ToString()}{res}";

            return res.ToString();
        }
    }
}