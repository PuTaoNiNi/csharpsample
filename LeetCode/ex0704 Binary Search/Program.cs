﻿// See https://aka.ms/new-console-template for more information

var solution = new Solution();

var nums1 = new int[] { -1, 0, 3, 5, 9, 12 };
var target1 = 9;
var output1 = solution.Search(nums1, target1);
Console.WriteLine(output1);

var nums2 = new int[] { -1, 0, 3, 5, 9, 12 };
var target2 = 2;
var output2 = solution.Search(nums2, target2);
Console.WriteLine(output2);

public class Solution
{
    public int Search(int[] nums, int target)
    {
        //var test = (1 + 2) >> 1;
        //var test1 = (1 + 2) / 2;
        var left = 0;
        var right = nums.Length - 1;

        while (left <= right)
        {
            var index = (left + right) / 2;

            if (nums[index] == target)
            {
                return index;
            }
            else if (nums[index] > target)
            {
                right = index - 1;
            }
            else
            {
                left = index + 1;
            }
        }

        return -1;
    }
}