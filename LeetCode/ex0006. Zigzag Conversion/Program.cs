﻿// See https://aka.ms/new-console-template for more information
using System.Text;

var solution = new Solution();

var s1 = "PAYPALISHIRING";
var numRows1 = 3;
var output1 = solution.Convert(s1, numRows1);
Console.WriteLine(output1);

var s2 = "PAYPALISHIRING";
var numRows2 = 4;
var output2 = solution.Convert(s2, numRows2);
Console.WriteLine(output2);

public class Solution
{
    public string Convert(string s, int numRows)
    {
        if (numRows == 1)
        {
            return s;
        }


        var n = (numRows - 1) * 2;
        var result = new StringBuilder();

        for (int i = 0; i < numRows; i++)
        {
            for (int j = i; j < s.Length; j = j + n)
            {
                result.Append(s[j]);

                if (i != 0 && i != numRows - 1 && j + (n - 2 * i) < s.Length)
                {
                    result.Append(s[j + (n - 2 * i)]);
                }
            }
        }

        return result.ToString();
    }
}