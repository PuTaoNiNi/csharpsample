﻿// See https://aka.ms/new-console-template for more information

var solution = new Solution();

var input1 = "bcabc";
var output1 = solution.RemoveDuplicateLetters(input1);
Console.WriteLine(output1);

var input2 = "cbacdcbc";
var output2 = solution.RemoveDuplicateLetters(input2);
Console.WriteLine(output2);

var input3 = "bbcaac";
var output3 = solution.RemoveDuplicateLetters(input3);
Console.WriteLine(output3);

public class Solution
{
    public string RemoveDuplicateLetters(string s)
    {
        var chars = new int[26];

        foreach (var c in s)
        {
            chars[c - 'a']++;
        }

        var index = 0;
        for (int i = 0; i < s.Length; i++)
        {
            if (s[i] < s[index])
            {
                index = i;
            }

            if (--chars[s[i] - 'a'] == 0)
            {
                break;
            }
        }

        return !s.Any() ? string.Empty : s[index] + RemoveDuplicateLetters(s.Substring(index + 1).Replace(s[index].ToString(), string.Empty));
    }
}


public class Solution1
{
    public string RemoveDuplicateLetters(string s)
    {
        Stack<char> stack = new Stack<char>();

        // this lets us keep track of what's in our solution in O(1) time
        HashSet<char> seen = new HashSet<char>();
        // this will let us know if there are any more instances of s[i] left in s
        var last_occurrence = new Dictionary<char, int>();
        for (var i = 0; i < s.Length; i++)
        {
            if (!last_occurrence.ContainsKey(s[i]))
            {
                last_occurrence.Add(s[i], -1);
            }
            last_occurrence[s[i]] = i;
        }
        for (int i = 0; i < s.Length; i++)
        {
            char c = s[i];
            // we can only try to add c if it's not already in our solution
            // this is to maintain only one of each character
            if (!seen.Contains(c))
            {
                // if the last letter in our solution:
                //     1. exists
                //     2. is greater than c so removing it will make the string smaller
                //     3. it's not the last occurrence
                // we remove it from the solution to keep the solution optimal
                while (stack.Any() && c < stack.Peek() && last_occurrence[stack.Peek()] > i)
                {
                    seen.Remove(stack.Pop());
                }
                seen.Add(c);
                stack.Push(c);
            }
        }
        var list = stack.ToList();
        list.Reverse();
        return new string(list.ToArray());
    }
}
