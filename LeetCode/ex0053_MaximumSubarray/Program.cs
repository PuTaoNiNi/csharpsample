﻿// See https://aka.ms/new-console-template for more information
var solution = new Solution();

var input1_nums = new int[] { -2, 1, -3, 4, -1, 2, 1, -5, 4 };
var output1 = solution.MaxSubArray(input1_nums);
Console.WriteLine(output1);

var input2_nums = new int[] { 1 };
var output2 = solution.MaxSubArray(input2_nums);
Console.WriteLine(output2);

var input3_nums = new int[] { 5, 4, -1, 7, 8 };
var output3 = solution.MaxSubArray(input3_nums);
Console.WriteLine(output3);

var input4_nums = new int[] { 8, -19, 5, -4, 20 };
var output4 = solution.MaxSubArray(input4_nums);
Console.WriteLine(output4);


public class Solution
{
    public int MaxSubArray(int[] nums)
    {
        var currentMax = 0;
        var max = nums[0];

        foreach (var num in nums)
        {
            currentMax += num;

            if (currentMax > max)
                max = currentMax;

            if (currentMax < 0)
                currentMax = 0;
        }

        return max;
    }
}

public class Solution1
{
    public int MaxSubArray(int[] nums)
    {
        int currMax = nums[0], max = nums[0];
        for (int i = 1; i < nums.Count(); i++)
        {
            currMax = Math.Max(currMax + nums[i], nums[i]);
            max = Math.Max(max, currMax);
        }
        return max;
    }
}