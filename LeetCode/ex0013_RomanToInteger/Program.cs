﻿var solution = new Solution1();
var output1 = solution.RomanToInt("III");
Console.WriteLine(output1);

var output2 = solution.RomanToInt("LVIII");
Console.WriteLine(output2);

var output3 = solution.RomanToInt("MCMXCIV");
Console.WriteLine(output3);

public class Solution
{
    public int RomanToInt(string s)
    {
        var result = 0;
        for (var i = 0; i < s.Length; i++)
        {
            var temp = 0;
            var twoChr = (i + 1) < s.Length ? s[i + 1] : ' ';

            switch (s[i])
            {
                case 'I':
                    temp = 1;

                    if (twoChr == 'V')
                    {
                        temp = 4;
                        i++;
                    }

                    if (twoChr == 'X')
                    {
                        temp = 9;
                        i++;
                    }

                    break;

                case 'V':
                    temp = 5;
                    break;
                case 'X':
                    temp = 10;

                    if (twoChr == 'L')
                    {
                        temp = 40; i++;
                    }

                    if (twoChr == 'C')
                    {
                        temp = 90; i++;
                    }

                    break;


                case 'L':
                    temp = 50;
                    break;

                case 'C':
                    temp = 100;

                    if (twoChr == 'D')
                    {
                        temp = 400; i++;
                    }

                    if (twoChr == 'M')
                    {
                        temp = 900; i++;
                    }

                    break;

                case 'D':
                    temp = 500;
                    break;

                case 'M':
                    temp = 1000;
                    break;
            }

            result = result + temp;
        }

        return result;
    }
}

public class Solution1
{
    public int ConvertSymboltoVal(char x)
    {
        switch (x)
        {
            case 'I':
                return 1;
            case 'V':
                return 5;
            case 'X':
                return 10;
            case 'L':
                return 50;
            case 'C':
                return 100;
            case 'D':
                return 500;
            case 'M':
                return 1000;
            default:
                return 0;
        }
    }

    public int RomanToInt(string s)
    {
        var result = 0;

        for (var i = 0; i < s.Length; i++)
        {
            var temp = ConvertSymboltoVal(s[i]);

            if (i == s.Length - 1 || temp >= ConvertSymboltoVal(s[i + 1]))
                result += temp;
            else
                result -= temp;
        }

        return result;
    }
}

public class Solution2
{
    readonly Dictionary<char, int> dict = new Dictionary<char, int>()
        {
            {'I', 1},
            {'V', 5},
            {'X', 10},
            {'L', 50},
            {'C', 100},
            {'D', 500},
            {'M', 1000}
        };

    public int RomanToInt(string s)
    {
        var result = 0;

        for (var i = 0; i < s.Length; i++)
        {
            var temp = dict[s[i]];

            if (i == s.Length - 1 || temp >= dict[s[i + 1]])
                result += temp;
            else
                result -= temp;
        }

        return result;
    }
}
