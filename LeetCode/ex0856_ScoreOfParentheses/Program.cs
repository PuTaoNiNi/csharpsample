﻿// See https://aka.ms/new-console-template for more information

var solution = new Solution();

var s1 = "()";
var output1 = solution.ScoreOfParentheses(s1);
Console.WriteLine(output1);

var s2 = "(())";
var output2 = solution.ScoreOfParentheses(s2);
Console.WriteLine(output2);

var s3 = "()()";
var output3 = solution.ScoreOfParentheses(s3);
Console.WriteLine(output3);

var s4 = "(()(()))";
var output4 = solution.ScoreOfParentheses(s4);
Console.WriteLine(output4);


public class Solution
{
    public int ScoreOfParentheses(string s)
    {
        var stack = new Stack<int>();
        var result = 0;
        foreach (var c in s)
        {
            if (c == '(')
            {
                stack.Push(result);
                result = 0;
            }
            else
            {
                result = stack.Pop() + Math.Max(result * 2, 1);
            }
        }

        return result;
    }
}