﻿// See https://aka.ms/new-console-template for more information

var solution = new Solution();

var n1 = 19;
var output1 = solution.IsHappy(n1);
Console.WriteLine(output1);

var n2 = 2;
var output2 = solution.IsHappy(n2);
Console.WriteLine(output2);

var n3 = 1111111;
var output3 = solution.IsHappy(n3);
Console.WriteLine(output3);

public class Solution
{
    public bool IsHappy(int n)
    {
        var hash = new HashSet<int>();
        while (n != 1)
        {
            if (hash.Contains(n)) return false;

            hash.Add(n);

            int newNum = 0;
            while (n > 0)
            {
                int d = n % 10;
                n = n / 10;
                newNum += d * d;
            }
            n = newNum;
        }

        return true;
    }
}


public class Solution1
{
    public bool IsHappy(int n)
    {
        while (n != 1 && n != 4)
        {
            n = Test(n);
        }

        return n == 1;
    }

    private int Test(int n)
    {
        int totalSum = 0;

        while (n > 0)
        {
            int d = n % 10;
            n = n / 10;
            totalSum += d * d;
        }

        return totalSum;
    }
}
