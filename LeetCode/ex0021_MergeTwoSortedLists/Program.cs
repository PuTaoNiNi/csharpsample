﻿// See https://aka.ms/new-console-template for more information
using ModelLibrary;

var solution = new Solution();

ListNode input1_l1 = new(1, new(2, new(4)));
ListNode input1_12 = new(1, new(3, new(4)));
var output1 = solution.MergeTwoLists(input1_l1, input1_12);
Console.WriteLine(output1.ToString());

ListNode input2_l1 = new(2);
ListNode input2_12 = new(1);
var output2 = solution.MergeTwoLists(input2_l1, input2_12);
Console.WriteLine(output2.ToString());

ListNode input3_l1 = null;
ListNode input3_12 = null;
var output3 = solution.MergeTwoLists(input3_l1, input3_12);
Console.WriteLine(output3?.ToString());



public class Solution
{
    public ListNode MergeTwoLists(ListNode list1, ListNode list2)
    {
        if (list1 == null)
        {
            return list2;
        }

        if (list2 == null)
        {
            return list1;
        }

        var result = new ListNode();
        var current = result;

        do
        {
            current.next = new();
            current = current.next;

            if (list1 == null || (list2 != null && list1.val > list2.val))
            {
                current.val = list2.val;
                list2 = list2.next;
            }
            else
            {
                current.val = list1.val;
                list1 = list1.next;
            }

        } while (list1 != null || list2 != null);

        return result.next;
    }
}

public class Solution1
{
    public ListNode MergeTwoLists(ListNode list1, ListNode list2)
    {
        var result = new ListNode();
        var current = result;

        while (list1 != null && list2 != null)
        {
            if (list1.val <= list2.val)
            {
                current.next = list1;
                list1 = list1.next;
            }
            else
            {
                current.next = list2;
                list2 = list2.next;
            }

            current = current.next;
        }

        if (list1 != null)
            current.next = list1;
        else
            current.next = list2;

        return result.next;
    }
}

public class Solution2
{
    public ListNode MergeTwoLists(ListNode list1, ListNode list2)
    {
        if (list1 == null) return list2;
        if (list2 == null) return list1;

        if (list1.val <= list2.val)
        {
            list1.next = MergeTwoLists(list1.next, list2);
            return list1;
        }
        else
        {
            list2.next = MergeTwoLists(list1, list2.next);
            return list2;
        }
    }
}

