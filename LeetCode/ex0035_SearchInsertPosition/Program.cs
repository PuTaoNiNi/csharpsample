﻿// See https://aka.ms/new-console-template for more information
var solution = new Solution1();

var input1_nums = new int[] { 1, 3, 5, 6 };
var input1_target = 5;
var output1 = solution.SearchInsert(input1_nums, input1_target);
Console.WriteLine(output1);

var input2_nums = new int[] { 1, 3, 5, 6 };
var input2_target = 2;
var output2 = solution.SearchInsert(input2_nums, input2_target);
Console.WriteLine(output2);

var input3_nums = new int[] { 1, 3, 5, 6 };
var input3_target = 7;
var output3 = solution.SearchInsert(input3_nums, input3_target);
Console.WriteLine(output3);

var input4_nums = new int[] { 5, 5, 5 };
var input4_target = 5;
var output4 = solution.SearchInsert(input4_nums, input4_target);
Console.WriteLine(output4);


public class Solution
{
    public int SearchInsert(int[] nums, int target)
    {
        var left = 0;
        var reight = nums.Length - 1;

        while (reight - left > 1)
        {
            var index = (left + reight) / 2;

            if (nums[index] >= target)
            {
                reight = index;
            }
            else if (nums[index] < target)
            {
                left = index;
            }
        }

        return target > nums[reight] ? reight + 1 : nums[left] >= target ? left : left + 1;
    }
}

public class Solution1
{
    public int SearchInsert(int[] nums, int target)
    {
        var left = 0;
        var right = nums.Length - 1;

        while (left <= right) // 4 <= 3
        {
            int mid = (left + right) / 2;

            if (nums[mid] < target)
            {
                left = mid + 1;
            }
            else
            {
                right = mid - 1;
            }
        }

        return left;
    }
}