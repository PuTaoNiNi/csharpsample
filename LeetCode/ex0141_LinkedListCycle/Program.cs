﻿// See https://aka.ms/new-console-template for more information
using ModelLibrary;

var solution = new Solution();

ListNode head1 = new(3, new(2, new(0, new(-4))));
head1.next.next.next.next = head1.next;
var output1 = solution.HasCycle(head1);
Console.WriteLine(output1.ToString());

ListNode head2 = new(1, new(2));
head2.next = head2;
var output2 = solution.HasCycle(head2);
Console.WriteLine(output2.ToString());

ListNode head3 = new(1);

var output3 = solution.HasCycle(head3);
Console.WriteLine(output3.ToString());


public class Solution
{
    public bool HasCycle(ListNode head)
    {
        if (head == null) return false;

        var jogger = head;
        var runner = head;

        while (runner.next != null && runner.next.next != null)
        {
            jogger = jogger.next;
            runner = runner.next.next;

            if (jogger == runner) return true;
        }

        return false;
    }
}