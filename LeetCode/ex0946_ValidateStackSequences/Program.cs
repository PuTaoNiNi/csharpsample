﻿// See https://aka.ms/new-console-template for more information

var solution = new Solution1();

int[] pushed1 = new int[] { 1, 2, 3, 4, 5 }, popped1 = new int[] { 4, 5, 3, 2, 1 };
var output1 = solution.ValidateStackSequences(pushed1, popped1);
Console.WriteLine(output1);

int[] pushed2 = new int[] { 1, 2, 3, 4, 5 }, popped2 = new int[] { 4, 3, 5, 1, 2 };
var output2 = solution.ValidateStackSequences(pushed2, popped2);
Console.WriteLine(output2);

public class Solution
{
    public bool ValidateStackSequences(int[] pushed, int[] popped)
    {
        var stack1 = new Stack<int>();
        var stack2 = new Stack<int>(popped.Reverse());

        foreach (var item in pushed)
        {
            if (stack2.Peek() == item)
            {
                stack2.Pop();

                while (stack1.Any() && stack1.Peek() == stack2.Peek())
                {
                    stack1.Pop();
                    stack2.Pop();
                }
            }
            else
            {
                stack1.Push(item);
            }
        }

        return !stack2.Any();
    }
}

public class Solution1
{
    public bool ValidateStackSequences(int[] pushed, int[] popped)
    {
        var stack = new Stack<int>();

        int i = 0;

        foreach (var item in pushed)
        {
            stack.Push(item);
            while (stack.Any() && stack.Peek() == popped[i])
            {
                stack.Pop();
                i++;
            }
        }

        return !stack.Any();
    }
}