﻿// See https://aka.ms/new-console-template for more information

using ModelLibrary;

var solution = new Solution();

ListNode head1 = new(1, new(1, new(2)));
var output1 = solution.DeleteDuplicates(head1);
Console.WriteLine(output1.ToString());

ListNode head2 = new(1, new(1, new(1, new(2, new(3, new(3))))));
var output2 = solution.DeleteDuplicates(head2);
Console.WriteLine(output2.ToString());

ListNode head3 = new(1, new(1, new(2, new(2))));
var output3 = solution.DeleteDuplicates(head3);
Console.WriteLine(output3.ToString());

public class Solution
{
    public ListNode DeleteDuplicates(ListNode head)
    {
        var result = new ListNode(0, head);
        var current = head;

        while (head != null && head.next != null)
        {
            if (head.val == head.next.val)
            {
                head = head.next;

                while (head.next != null && head.val == head.next.val)
                {
                    head = head.next;
                }

                current.next = head.next;
                current = head.next;
            }
            else
            {
                current = head.next;
            }


            head = head.next;
        }

        return result.next;
    }
}


public class Solution1
{
    public ListNode DeleteDuplicates(ListNode head)
    {
        ListNode first = head;

        while (head != null && head.next != null)
        {
            if (head.val == head.next.val)
            {
                head.next = head.next.next;
            }
            else
            {
                head = head.next;
            }
        }

        return first;
    }
}