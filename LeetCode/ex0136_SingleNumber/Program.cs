﻿// See https://aka.ms/new-console-template for more information
using ModelLibrary;

var solution = new Solution1();

ListNode input1 = new(1, new(2, new(3, new(4))));
var output1 = solution.SwapPairs(input1);
Console.WriteLine(output1.ToString());

ListNode input2 = null;
var output2 = solution.SwapPairs(input2);
Console.WriteLine(output2?.ToString());

ListNode input3 = new(1);
var output3 = solution.SwapPairs(input3);
Console.WriteLine(output3.ToString());

public class Solution
{
    public ListNode SwapPairs(ListNode head)
    {
        if (head == null)
        {
            return head;
        }

        ListNode upNode = null;
        var node1 = head;
        var node2 = head?.next;

        while (node2 != null)
        {
            var tempNode = node2?.next;

            node2.next = node1;
            node1.next = tempNode;

            if (upNode != null)
            {
                upNode.next = node2;
            }

            if (head == node1)
            {
                head = node2;
            }

            upNode = node1;
            node1 = tempNode;
            node2 = tempNode?.next;
        }

        return head;
    }
}


public class Solution1
{
    public ListNode SwapPairs(ListNode head)
    {
        ListNode r = new ListNode(-1, head);

        ListNode prev = r;
        ListNode first = head;
        ListNode second;

        while (first != null && first.next != null)
        {
            second = first.next;

            first.next = second.next;
            second.next = first;
            prev.next = second;

            prev = first;
            first = first.next;
        }

        return r.next;
    }
}