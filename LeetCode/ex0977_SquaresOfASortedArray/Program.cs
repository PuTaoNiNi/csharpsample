﻿// See https://aka.ms/new-console-template for more information

var solution = new Solution();

int[] nums1 = new int[] { -4, -1, 0, 3, 10 };
var output1 = solution.SortedSquares(nums1);
Console.WriteLine($"[{string.Join(",", output1)}]");

int[] nums2 = new int[] { -7, -3, 2, 3, 11 };
var output2 = solution.SortedSquares(nums2);
Console.WriteLine($"[{string.Join(",", output2)}]");

public class Solution
{
    public int[] SortedSquares(int[] nums)
    {
        var result = new int[nums.Length];

        int f = 0, l = nums.Length - 1;
        for (int i = nums.Length - 1; i >= 0; i--)
        {
            if (Math.Abs(nums[f]) > Math.Abs(nums[l]))
            {
                result[i] = nums[f] * nums[f];
                f++;
            }
            else
            {
                result[i] = nums[l] * nums[l];
                l--;
            }
        }

        return result;
    }
}