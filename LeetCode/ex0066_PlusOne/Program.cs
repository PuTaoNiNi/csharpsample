﻿// See https://aka.ms/new-console-template for more information
var solution = new Solution();

var input1_nums = new int[] { 1, 2, 3 };
var output1 = solution.PlusOne(input1_nums);
Console.WriteLine(string.Join(", ", output1.Select(x => x)));

var input2_nums = new int[] { 4, 3, 2, 1 };
var output2 = solution.PlusOne(input2_nums);
Console.WriteLine(string.Join(", ", output2.Select(x => x)));

var input3_nums = new int[] { 9 };
var output3 = solution.PlusOne(input3_nums);
Console.WriteLine(string.Join(", ", output3.Select(x => x)));

var input4_nums = new int[] { 9, 9 };
var output4 = solution.PlusOne(input4_nums);
Console.WriteLine(string.Join(", ", output4.Select(x => x)));

public class Solution
{
    public int[] PlusOne(int[] digits)
    {
        var index = digits.Length - 1;
        digits[index] = (digits[index] + 1);

        for (int i = index; i >= 0; i--)
        {
            if (digits[i] == 10)
            {
                digits[i] = digits[i] % 10;

                if (i - 1 >= 0)
                {
                    digits[i - 1] = digits[i - 1] + 1;
                }
                else
                {
                    digits = digits.Reverse().Append(1).Reverse().ToArray();
                }
            }
        }

        return digits;
    }
}

public class Solution1
{
    public int[] PlusOne(int[] digits)
    {
        for (int i = digits.Length - 1; i >= 0; i--)
        {
            if (digits[i] < 9)
            {
                digits[i]++;

                return digits;
            }

            digits[i] = 0;
        }

        digits = new int[digits.Length + 1];
        digits[0] = 1;

        return digits;
    }
}