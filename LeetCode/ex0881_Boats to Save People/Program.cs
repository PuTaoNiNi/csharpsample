﻿// See https://aka.ms/new-console-template for more information

var solution = new Solution1();

//var people1 = new int[] { 1, 2 };
//var limit1 = 3;
//var output1 = solution.NumRescueBoats(people1, limit1);
//Console.WriteLine(output1);

var people2 = new int[] { 3, 5, 3, 4 };
var limit2 = 5;
var output2 = solution.NumRescueBoats(people2, limit2);
Console.WriteLine(output2);

//var people3 = new int[] { 3, 5, 3, 4 };
//var limit3 = 5;
//var output3 = solution.NumRescueBoats(people3, limit3);
//Console.WriteLine(output3);

//var people4 = new int[] { 1, 5, 3, 5 };
//var limit4 = 7;
//var output4 = solution.NumRescueBoats(people4, limit4);
//Console.WriteLine(output4);

//var people5 = new int[] { 21, 40, 16, 24, 30 };
//var limit5 = 50;
//var output5 = solution.NumRescueBoats(people5, limit5);
//Console.WriteLine(output5);


public class Solution
{
    public int NumRescueBoats(int[] people, int limit)
    {
        Array.Sort(people);
        var result = 0;
        var i = 0;
        var j = people.Length - 1;

        while (i <= j)
        {
            result++;
            if (people[i] + people[j] <= limit)
                i++;

            j--;
        }

        return result;
    }
}


public class Solution1
{
    public int NumRescueBoats(int[] people, int limit)
    {
        Array.Sort(people);
        int j = people.Length - 1;
        for (int i = 0; i <= j; j--)
        {
            if (people[i] + people[j] <= limit) i++;
        }
        return people.Length - 1 - j;
    }
}