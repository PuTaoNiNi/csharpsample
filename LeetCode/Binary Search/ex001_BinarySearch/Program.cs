﻿// See https://aka.ms/new-console-template for more information
var solution = new Solution();

var input1_nums = new int[] { -1, 0, 3, 5, 9, 12 };
var input1_target = 5;
var output1 = solution.Search(input1_nums, input1_target);
Console.WriteLine(output1);

var input2_nums = new int[] { -1, 0, 3, 5, 9, 12 };
var input2_target = 2;
var output2 = solution.Search(input2_nums, input2_target);
Console.WriteLine(output2);

public class Solution
{
    public int Search(int[] nums, int target)
    {
        var start = 0;
        var end = nums.Length - 1;
        int mid;

        while (start <= end)
        {
            mid = (start + end) / 2;

            if (nums[mid] < target)
                start = mid + 1;
            else if (nums[mid] > target)
                end = mid - 1;
            else
                return mid;
        }

        return -1;
    }
}