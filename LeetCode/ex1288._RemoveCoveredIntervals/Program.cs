﻿// See https://aka.ms/new-console-template for more information
var solution = new Solution();

var input1 = new int[][] { new[] { 1, 4 }, new[] { 3, 6 }, new[] { 2, 8 } };
var output1 = solution.RemoveCoveredIntervals(input1);
Console.WriteLine(output1);

var input2 = new int[][] { new[] { 1, 4 }, new[] { 2, 3 } };
var output2 = solution.RemoveCoveredIntervals(input2);
Console.WriteLine(output2);


public class Solution
{
    public int RemoveCoveredIntervals(int[][] intervals)
    {
        Array.Sort(intervals, (x, y) =>
        {
            if (x[0] == y[0])
                return y[1] - x[1];
            return x[0] - y[0];
        });

        var overlapCount = 0;
        int[] overlappingInterval = null;
        for (int i = 0; i < intervals.Length; i++)
        {
            var current = intervals[i];
            if (overlappingInterval is null)
            {
                overlappingInterval = current;
                continue;
            }

            if (current[0] >= overlappingInterval[0] && current[1] <= overlappingInterval[1])
            {
                overlapCount++;
            }
            else
            {
                overlappingInterval = current;
            }
        }

        return intervals.Length - overlapCount;
    }
}