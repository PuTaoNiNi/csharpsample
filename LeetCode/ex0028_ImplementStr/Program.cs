﻿// See https://aka.ms/new-console-template for more information
var solution = new Solution2();

var input1_haystack = "hello";
var input1_needle = "ll";
var output1 = solution.StrStr(input1_haystack, input1_needle);
Console.WriteLine(output1);

var input2_haystack = "aaaaa";
var input2_needle = "bba";
var output2 = solution.StrStr(input2_haystack, input2_needle);
Console.WriteLine(output2);

var input3_haystack = "";
var input3_needle = "";
var output3 = solution.StrStr(input3_haystack, input3_needle);
Console.WriteLine(output3);

var input4_haystack = "i1ii";
var input4_needle = "ii";
var output4 = solution.StrStr(input4_haystack, input4_needle);
Console.WriteLine(output4);

public class Solution
{
    public int StrStr(string haystack, string needle)
    {
        return haystack.IndexOf(needle);
    }
}


public class Solution1
{
    public int StrStr(string haystack, string needle)
    {
        if (needle.Length == 0)
        {
            return 0;
        }

        for (int i = 0; i <= (haystack.Length - needle.Length); i++)
        {
            if (needle[0] == haystack[i])
            {
                for (int j = needle.Length - 1; j >= 0; j--)
                {
                    if (j == 0)
                        return i;

                    if (haystack[i + j] != needle[j])
                        break;
                }
            }
        }
        return -1;
    }
}

// KMP pattern matching
// https://ithelp.ithome.com.tw/articles/10245444
// https://www.geeksforgeeks.org/kmp-algorithm-for-pattern-searching/
public class Solution2
{
    public int StrStr(string haystack, string needle)
    {
        return 0;
    }
}