﻿// See https://aka.ms/new-console-template for more information
using System.Text;

var solution = new Solution();

var input1 = 4;
var output1 = solution.GenerateTheString(input1);
Console.WriteLine(output1);

var input2 = 2;
var output2 = solution.GenerateTheString(input2);
Console.WriteLine(output2);

var input3 = 7;
var output3 = solution.GenerateTheString(input3);
Console.WriteLine(output3);


public class Solution
{
    public string GenerateTheString(int n)
    {
        var result = new StringBuilder();
        for (int i = 0; i < n; i++)
        {
            result.Append('a');
        }

        if (n % 2 == 0)
        {
            result[n - 1] = 'b';
        }

        return result.ToString();
    }
}