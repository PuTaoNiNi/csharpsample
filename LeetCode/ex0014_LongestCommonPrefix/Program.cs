﻿// TODO:
// 1: Horizontal scanning OK
// 2: Vertical scanning
// 3: Divide and conquer
// 4: Binary search

var solution = new Solution1();

var input1 = new string[] { "flower", "flow", "flight" };
var output1 = solution.LongestCommonPrefix(input1);
Console.WriteLine(output1);

var input2 = new string[] { "dog", "racecar", "car" };
var output2 = solution.LongestCommonPrefix(input2);
Console.WriteLine(output2);

var input3 = new string[] { "flower", "flower", "flower", "flower" };
var output3 = solution.LongestCommonPrefix(input3);
Console.WriteLine(output3);

var input4 = new string[] { "" };
var output4 = solution.LongestCommonPrefix(input4);
Console.WriteLine(output4);

var input5 = new string[] { "", "", "", "" };
var output5 = solution.LongestCommonPrefix(input5);
Console.WriteLine(output5);

public class Solution
{
    public string LongestCommonPrefix(string[] strs)
    {
        string tempStr = null;

        if (strs.Length == 1 || strs[0] == string.Empty)
            return strs[0];

        for (var i = 0; i < strs.Length - 1; i++)
        {
            var str1 = tempStr ?? strs[i];
            var str2 = strs[i + 1];

            for (var j = 1; j <= str1.Length; j++)
            {
                if (!str2.StartsWith(str1.Substring(0, j)))
                {
                    tempStr = str1.Substring(0, j - 1);
                    break;
                }

                if (j == str1.Length)
                    tempStr = str1;
            }
        }

        return tempStr ?? "";
    }
}

public class Solution1
{
    public string LongestCommonPrefix(string[] strs)
    {
        if (strs.Length == 0)
            return string.Empty;

        var prefix = strs[0];

        for (var i = 1; i < strs.Length; i++)
        {
            while (strs[i].IndexOf(prefix) != 0)
            {
                prefix = prefix.Substring(0, prefix.Length - 1);
            }
        }

        return prefix;
    }
}