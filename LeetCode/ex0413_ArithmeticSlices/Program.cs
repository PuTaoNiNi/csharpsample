﻿// See https://aka.ms/new-console-template for more information

var solution = new Solution();

var input1 = new int[] { 1, 2, 3, 4, 5 };
var output1 = solution.NumberOfArithmeticSlices(input1);
Console.WriteLine(output1);

var input2 = new int[] { 1, 2, 3, 4 };
var output2 = solution.NumberOfArithmeticSlices(input2);
Console.WriteLine(output2);

var input3 = new int[] { 1, 2, 3, 5, 6, 7 };
var output3 = solution.NumberOfArithmeticSlices(input3);
Console.WriteLine(output3);

var input4 = new int[] { 1 };
var output4 = solution.NumberOfArithmeticSlices(input4);
Console.WriteLine(output4);


public class Solution
{
    public int NumberOfArithmeticSlices(int[] nums)
    {
        var result = 0;

        if (nums.Length < 3)
        {
            return result;
        }

        var count = 0;
        var temp = nums[0] - nums[1];

        for (var i = 1; i < nums.Length - 1; i++)
        {
            if (temp == nums[i] - nums[i + 1])
            {
                count++;
            }
            else
            {
                temp = nums[i] - nums[i + 1];
                count = 0;
            }

            result = result + count;
        }

        return result;
    }
}

public class Solution1
{
    public int NumberOfArithmeticSlices(int[] nums)
    {
        var result = 0;

        if (nums.Length < 3)
        {
            return result;
        }

        var count = 0;
        var temp = nums[0] - nums[1];

        for (var i = 1; i < nums.Length - 1; i++)
        {
            if (temp == nums[i] - nums[i + 1])
            {
                count++;
            }
            else
            {
                temp = nums[i] - nums[i + 1];
                count = 0;
            }

            result = result + count;
        }

        return result;
    }
}