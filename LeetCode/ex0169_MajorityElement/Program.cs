﻿// See https://aka.ms/new-console-template for more information

var solution = new Solution1();

//var input1 = new int[] { 3, 2, 3 };
//var output1 = solution.MajorityElement(input1);
//Console.WriteLine(output1);

//var input2 = new int[] { 2, 2, 1, 1, 1, 2, 2 };
//var output2 = solution.MajorityElement(input2);
//Console.WriteLine(output2);

var input3 = new int[] { 6, 5, 5 };
var output3 = solution.MajorityElement(input3);
Console.WriteLine(output3);



// Boyer–Moore majority vote algorithm
public class Solution
{
    public int MajorityElement(int[] nums)
    {
        var dic = new Dictionary<int, int>();

        for (int i = 0; i < nums.Length; i++)
        {
            if (dic.ContainsKey(nums[i]))
                dic[nums[i]]++;
            else
                dic.Add(nums[i], 1);
        }

        return dic.Single(x => x.Value > nums.Length / 2).Key;
    }
}

public class Solution1
{
    public int MajorityElement(int[] nums)
    {
        int res = nums[0], cnt = 1;

        // Boyer-Moore Voting Algorithm
        for (int i = 1; i < nums.Length; ++i)
        {
            if (res == nums[i])
            {
                cnt++;
            }
            else if (cnt > 0)
            {
                cnt--;
            }
            else
            {
                res = nums[i];
                cnt++;
            }
        }

        return res;
    }
}