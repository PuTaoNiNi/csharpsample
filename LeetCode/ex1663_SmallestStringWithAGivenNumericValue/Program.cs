﻿// See https://aka.ms/new-console-template for more information

var solution = new Solution();

var n1 = 3;
var k1 = 27;
var output1 = solution.GetSmallestString(n1, k1);
Console.WriteLine(output1);

var n2 = 5;
var k2 = 73;
var output2 = solution.GetSmallestString(n2, k2);
Console.WriteLine(output2);

var n3 = 53168;
var k3 = 485414;
var output3 = solution.GetSmallestString(n3, k3);
Console.WriteLine(output3);

public class Solution
{
    public string GetSmallestString(int n, int k)
    {
        var result = new char[n];
        Array.Fill(result, 'a');
        k = k - n;

        while (n > 0)
        {
            var lastCharValue = Math.Min(25, k);
            result[n - 1] = (char)('a' + lastCharValue);

            k -= lastCharValue;
            n--;
        }

        return new string(result);
    }
}