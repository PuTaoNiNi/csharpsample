﻿// See https://aka.ms/new-console-template for more information
using System.Text;

var solution = new Solution();

var num1 = 3;
var output1 = solution.IntToRoman(num1);
Console.WriteLine(output1);

var num2 = 58;
var output2 = solution.IntToRoman(num2);
Console.WriteLine(output2);

var num3 = 1994;
var output3 = solution.IntToRoman(num3);
Console.WriteLine(output3);

var num4 = 3999;
var output4 = solution.IntToRoman(num4);
Console.WriteLine(output4);

public class Solution
{
    protected static readonly int[] values = { 1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1 };
    protected static readonly string[] symbols = { "M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I" };

    public string IntToRoman(int num)
    {

        var result = new StringBuilder();
        var i = 0;

        while (i < values.Length)
        {
            if (num >= values[i])
            {
                num -= values[i];
                result.Append(symbols[i]);
            }
            else
            {
                i++;
            }
        }

        return result.ToString();
    }
}