﻿// See https://aka.ms/new-console-template for more information

var solution = new Solution();

var input1 = new int[] { 0, 1, 2, 4, 5, 7 };
var output1 = solution.SummaryRanges(input1);
Console.WriteLine(string.Join("\r\n", output1));

var input2 = new int[] { 0, 2, 3, 4, 6, 8, 9 };
var output2 = solution.SummaryRanges(input2);
Console.WriteLine(string.Join("\r\n", output2));

public class Solution
{
    public IList<string> SummaryRanges(int[] nums)
    {
        var result = new List<string>();
        if (nums.Length == 0)
        {
            return result;
        }

        var left = nums[0];
        var right = left + 1;

        for (var i = 1; i < nums.Length; i++)
        {
            if (right == nums[i])
            {
                right++;
            }
            else
            {
                right--;

                Append();

                left = nums[i];
                right = nums[i] + 1;
            }
        }

        right--;

        Append();

        void Append()
        {
            if (left == right)
                result.Add($"{left.ToString()}");
            else
                result.Add($"{left.ToString()}->{right.ToString()}");
        }

        return result;
    }
}

