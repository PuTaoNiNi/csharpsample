﻿// See https://aka.ms/new-console-template for more information
var solution = new Solution();

var input1_nums = "Hello World";
var output1 = solution.LengthOfLastWord(input1_nums);
Console.WriteLine(output1);

var input2_nums = "   fly me   to   the moon  ";
var output2 = solution.LengthOfLastWord(input2_nums);
Console.WriteLine(output2);

var input3_nums = "luffy is still joyboy";
var output3 = solution.LengthOfLastWord(input3_nums);
Console.WriteLine(output3);

public class Solution
{
    public int LengthOfLastWord(string s)
    {
        s = s.TrimEnd();

        var index = s.LastIndexOf(' ');

        return s.Length - index - 1;
    }
}

public class Solution1
{
    public int LengthOfLastWord(string s)
    {
        s = s.TrimEnd();

        var ans = 0;

        for (var i = s.Length - 1; i >= 0; i--)
        {
            if (s[i] == ' ')
                return ans;
            else
                ans++;
        }

        return ans;
    }
}