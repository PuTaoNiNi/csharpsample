﻿var solution = new Solution();

var output1 = solution.IsPalindrome(1);
Console.WriteLine(output1);

var output2 = solution.IsPalindrome(121);
Console.WriteLine(output2);

var output3 = solution.IsPalindrome(12344321);
Console.WriteLine(output3);

var output4 = solution.IsPalindrome(123414321);
Console.WriteLine(output4);

var output5 = solution.IsPalindrome(12345321);
Console.WriteLine(output5);

public class Solution
{
    public bool IsPalindrome(int x)
    {
        if (x < 0 || (x % 10 == 0 && x != 0))
            return false;
        else if (x % 10 == x)
            return true;

        var value2 = 0;

        while (x > 0)
        {
            var value1 = x % 10;

            var revertedValue = value2 * 10 + value1;

            x = x / 10;

            value2 = revertedValue;

            Console.WriteLine($"{x}, {revertedValue}");

            if (revertedValue == x || revertedValue == x / 10)
                return true;
        }

        return false;
    }
}

public class Solution1
{
    public bool IsPalindrome(int x)
    {
        if (x < 0)
            return false;

        var temp = x;
        var revertedValue = 0;

        while (temp > 0)
        {
            revertedValue = revertedValue * 10 + temp % 10;

            temp = temp / 10;
        }

        if (revertedValue == x)
            return true;

        return false;
    }
}

public class Solution2
{
    public bool IsPalindrome(int x)
    {
        if (x < 0 || (x % 10 == 0 && x != 0))
            return false;

        var revertedValue = 0;

        while (x > revertedValue)
        {
            revertedValue = revertedValue * 10 + x % 10;

            x = x / 10;

            Console.WriteLine($"{x}, {revertedValue}");
        }

        return x == revertedValue || x == revertedValue / 10;
    }
}