﻿var solution = new Solution1();

var input1 = "()";
var output1 = solution.IsValid(input1);
Console.WriteLine(output1);

var input2 = "()[]{}";
var output2 = solution.IsValid(input2);
Console.WriteLine(output2);

var input3 = "({{{{}}}})";
var output3 = solution.IsValid(input3);
Console.WriteLine(output3);

var input4 = "[{}{}{}]";
var output4 = solution.IsValid(input4);
Console.WriteLine(output4);

var input5 = "{[]}";
var output5 = solution.IsValid(input5);
Console.WriteLine(output5);

var input6 = "){";
var output6 = solution.IsValid(input6);
Console.WriteLine(output6);

var input7 = "([)]";
var output7 = solution.IsValid(input7);
Console.WriteLine(output7);

var input8 = "[";
var output8 = solution.IsValid(input8);
Console.WriteLine(output8);

var input9 = "(]";
var output9 = solution.IsValid(input9);
Console.WriteLine(output9);

var input10 = "(){}}{";
var output10 = solution.IsValid(input10);
Console.WriteLine(output10);

var input11 = "(}{)";
var output11 = solution.IsValid(input11);
Console.WriteLine(output11);

public class Solution
{
    public bool IsValid(string s)
    {
        var stack = new Stack<char>();

        for (int i = 0; i < s.Length; i++)
        {
            switch (s[i])
            {
                case ')':
                    if (stack.Count == 0 || stack.Pop() != '(')
                        return false;
                    break;
                case '}':
                    if (stack.Count == 0 || stack.Pop() != '{')
                        return false;
                    break;
                case ']':
                    if (stack.Count == 0 || stack.Pop() != '[')
                        return false;
                    break;

                default:
                    stack.Push(s[i]);
                    break;
            }
        }

        return stack.Count == 0;
    }
}

public class Solution1
{
    public bool IsValid(string s)
    {
        var stack = new Stack<char>();

        foreach (var c in s)
        {
            if (c == '(' || c == '[' || c == '{')
            {
                stack.Push(c);
            }
            else
            {
                if (stack.Count == 0)
                    return false;

                var left = stack.Pop();
                if ((left == '(' && c != ')') ||
                    (left == '{' && c != '}') ||
                    (left == '[' && c != ']'))
                    return false;
            }
        }

        return stack.Count == 0;
    }
}

