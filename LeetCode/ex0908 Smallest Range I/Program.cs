﻿// See https://aka.ms/new-console-template for more information

var solution = new Solution();

var nums1 = new int[] { 1 };
var k1 = 0;
var output1 = solution.SmallestRangeI(nums1, k1);
Console.WriteLine(output1);

var nums2 = new int[] { 0, 10 };
var k2 = 2;
var output2 = solution.SmallestRangeI(nums2, k2);
Console.WriteLine(output2);

var nums3 = new int[] { 1, 3, 6 };
var k3 = 3;
var output3 = solution.SmallestRangeI(nums3, k3);
Console.WriteLine(output3);


public class Solution
{
    public int SmallestRangeI(int[] nums, int k)
    {
        return Math.Max(0, nums.Max() - nums.Min() - 2 * k);
    }
}