﻿// See https://aka.ms/new-console-template for more information
using ModelLibrary;
var solution = new Solution();

TreeNode input1_l1 = new(1, new(2), new(3));
TreeNode input1_l2 = new(1, new(2), new(3));
var output1 = solution.IsSameTree(input1_l1, input1_l2);
Console.WriteLine(output1.ToString());

TreeNode input2_l1 = new(1, new(2), new(3));
TreeNode input2_l2 = new(1, null, new(2));
var output2 = solution.IsSameTree(input2_l1, input2_l2);
Console.WriteLine(output2.ToString());


TreeNode input3_l1 = new(1, new(2), new(1));
TreeNode input3_l2 = new(1, new(1), new(2));
var output3 = solution.IsSameTree(input3_l1, input3_l2);
Console.WriteLine(output3.ToString());

TreeNode input4_l1 = new(1, new(2, null, new TreeNode(2)), new(1));
TreeNode input4_l2 = new(1, new(2, null, new TreeNode(3)), new(1));
var output4 = solution.IsSameTree(input4_l1, input4_l2);
Console.WriteLine(output4.ToString());

TreeNode input5_l1 = null;
TreeNode input5_l2 = null;
var output5 = solution.IsSameTree(input5_l1, input5_l2);
Console.WriteLine(output5.ToString());

public class Solution
{
    // Approach 1: Recursion
    public bool IsSameTree(TreeNode p, TreeNode q)
    {
        if (p == null && q == null)
        {
            return true;
        }
        else if (p != null && q != null && p.val == q.val)
        {
            return IsSameTree(p.left, q.left) && IsSameTree(p.right, q.right);
        }
        else
        {
            return false;
        }
    }

    // Approach 2: Iteration
}