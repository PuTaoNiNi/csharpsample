﻿using System.Text;

namespace ModelLibrary
{
    public class ListNode
    {
        public int val;
        public ListNode next;
        public ListNode(int val = 0, ListNode next = null)
        {
            this.val = val;
            this.next = next;
        }

        public override string ToString()
        {
            var node = this;
            var result = new StringBuilder();

            result.Append($"{this.val}");

            while (node.next != null)
            {
                node = node.next;

                result.Append($", {node.val}");
            }

            return $"[{result.ToString()}]";
        }
    }
}