﻿using System.Text;

namespace ModelLibrary
{
    public class TreeNode
    {
        public int val;

        public TreeNode left;

        public TreeNode right;

        public TreeNode(int val = 0, TreeNode left = null, TreeNode right = null)
        {
            this.val = val;
            this.left = left;
            this.right = right;
        }

        public override string ToString()
        {
            var node = this;
            var result = new StringBuilder();

            result.Append($"{this.val}");

            while (node.left != null)
            {
                node = node.left;

                result.Append($", {node.val}");
            }

            return $"[{result.ToString()}]";
        }
    }
}
