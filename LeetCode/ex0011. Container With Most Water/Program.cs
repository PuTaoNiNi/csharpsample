﻿// See https://aka.ms/new-console-template for more information
var solution = new Solution1();

var height1 = new int[] { 1, 8, 6, 2, 5, 4, 8, 3, 7 };
var output1 = solution.MaxArea(height1);
Console.WriteLine(output1);

var height2 = new int[] { 1, 1 };
var output2 = solution.MaxArea(height2);
Console.WriteLine(output2);

public class Solution
{
    public int MaxArea(int[] height)
    {
        var result = 0;
        var j = 0;
        var n = height.Length - 1;
        for (int i = 0; i + j < height.Length; i++)
        {
            var cal = Math.Min(height[i], height[n - j]) * (n - i - j);
            result = Math.Max(result, cal);

            while (height[i] > height[n - j])
            {
                j++;

                cal = Math.Min(height[i], height[n - j]) * (n - i - j);
                result = Math.Max(result, cal);
            }
        }

        return result;
    }
}

public class Solution1
{
    public int MaxArea(int[] heights)
    {
        var result = 0;
        var i = 0;
        var j = heights.Length - 1;

        while (i < j)
        {
            var height = Math.Min(heights[i], heights[j]);
            var width = j - i;

            result = Math.Max(result, height * width);

            if (heights[i] > heights[j])
            {
                j--;
            }
            else
            {
                i++;
            }
        }

        return result;
    }
}