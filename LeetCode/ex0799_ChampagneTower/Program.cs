﻿// See https://aka.ms/new-console-template for more information

var solution = new Solution();

//int poured1 = 1, query_row1 = 1, query_glass1 = 1;
//var output1 = solution.ChampagneTower(poured1, query_row1, query_glass1);
//Console.WriteLine(output1);

//int poured2 = 2, query_row2 = 1, query_glass2 = 1;
//var output2 = solution.ChampagneTower(poured2, query_row2, query_glass2);
//Console.WriteLine(output2);


int poured3 = 25, query_row3 = 6, query_glass3 = 1;
var output3 = solution.ChampagneTower(poured3, query_row3, query_glass3);
Console.WriteLine(output3);


public class Solution
{
    //public double ChampagneTower(int poured, int query_row, int query_glass)
    //{
    //    var solution = new double[query_row + 1, query_row + 1];

    //    solution[0, 0] = poured;

    //    for (int i = 0; i < query_row; i++)
    //    {
    //        for (int j = 0; j <= i; j++)
    //        {
    //            if (solution[i, j] > 1)
    //            {
    //                solution[i + 1, j] += (solution[i, j] - 1) / 2.0;
    //                solution[i + 1, j + 1] = (solution[i, j] - 1) / 2.0;
    //                solution[i, j] = 1;
    //            }
    //        }
    //    }

    //    return Math.Min(solution[query_row, query_glass], 1);
    //}

    public double ChampagneTower(int poured, int query_row, int query_glass)
    {
        double[,] dp = new double[query_row + 1, query_row + 1];
        dp[0, 0] = poured;

        for (int i = 0; i < query_row; ++i)
        {
            for (int j = 0; j <= i; ++j)
            {
                if (dp[i, j] > 1)
                {
                    dp[i + 1, j] += (dp[i, j] - 1) / 2.0;
                    dp[i + 1, j + 1] += (dp[i, j] - 1) / 2.0;
                }
            }
        }

        return Math.Min(1.0, dp[query_row, query_glass]);
    }
}

public class Solution1
{

    double max(double a, double b)
    {
        if (b < a)
        {
            return a;
        }
        else
        {
            return b;
        }
    }
    double min(double a, double b)
    {
        if (b > a)
        {
            return a;
        }
        else
        {
            return b;
        }
    }

    public double ChampagneTower(int poured, int query_row, int query_glass)
    {

        int rectRightLength = query_glass + 1;
        int rectLeftLength = query_row - query_glass + 1;

        double[,] pouredInTotal = new double[rectRightLength, rectLeftLength];

        pouredInTotal[0, 0] = poured;

        for (int r = 1; r < rectRightLength; r++)
        {
            pouredInTotal[r, 0] = max(pouredInTotal[r - 1, 0] - 1, 0) / 2;
        }

        for (int l = 1; l < rectLeftLength; l++)
        {
            pouredInTotal[0, l] = max(pouredInTotal[0, l - 1] - 1, 0) / 2;
        }

        for (int l = 1; l < rectLeftLength; l++)
        {
            for (int r = 1; r < rectRightLength; r++)
            {
                pouredInTotal[r, l] = max(pouredInTotal[r, l - 1] - 1, 0) / 2 + max(pouredInTotal[r - 1, l] - 1, 0) / 2;
            }
        }

        return min(pouredInTotal[rectRightLength - 1, rectLeftLength - 1], 1);
    }
}