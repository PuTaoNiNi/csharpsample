﻿// See https://aka.ms/new-console-template for more information

var solution = new Solution();

var startValue1 = 2;
var target1 = 3;
var output1 = solution.BrokenCalc(startValue1, target1);
Console.WriteLine(output1);

var startValue2 = 5;
var target2 = 8;
var output2 = solution.BrokenCalc(startValue2, target2);
Console.WriteLine(output2);

var startValue3 = 3;
var target3 = 10;
var output3 = solution.BrokenCalc(startValue3, target3);
Console.WriteLine(output3);

public class Solution
{
    public int BrokenCalc(int startValue, int target)
    {
        var ans = 0;
        while (target > startValue)
        {
            target = target % 2 == 0 ? target / 2 : target + 1;
            ans++;
        }

        return ans + startValue - target;
    }
}