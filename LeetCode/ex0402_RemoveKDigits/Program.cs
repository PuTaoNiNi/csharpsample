﻿// See https://aka.ms/new-console-template for more information

var solution = new Solution2();

//var input1 = "1432219";
//var k1 = 3;
//var output1 = solution.RemoveKdigits(input1, k1);
//Console.WriteLine(output1);

var input2 = "10200";
var k2 = 1;
var output2 = solution.RemoveKdigits(input2, k2);
Console.WriteLine(output2);

//var input3 = "10";
//var k3 = 2;
//var output3 = solution.RemoveKdigits(input3, k3);
//Console.WriteLine(output3);

//var input4 = "112";
//var k4 = 1;
//var output4 = solution.RemoveKdigits(input4, k4);
//Console.WriteLine(output4);

public class Solution
{
    public string RemoveKdigits(string num, int k)
    {
        if (k >= num.Length) return "0";

        var result = string.Empty;
        var length = num.Length - k;
        foreach (var c in num)
        {
            while (k > 0 && result.Length > 0 && result[result.Length - 1] > c)
            {
                result = result.Remove(result.Length - 1);
                k--;
            }

            result = result + c;
        }

        result = result.Substring(0, length);
        result = result.TrimStart('0');

        return result.Length == 0 ? "0" : result;
    }
}

public class Solution1
{
    public string RemoveKdigits(string num, int k)
    {
        var length = num.Length;

        if (k >= length) return "0";

        var stack = new Stack<char>();
        var count = 0;
        while (count < num.Length)
        {
            while (k > 0 && stack.Count > 0 && stack.Peek() > num[count])
            {
                stack.Pop();
                k--;
            }

            stack.Push(num[count]);
            count++;
        }

        while (k > 0)
        {
            stack.Pop();
            k--;
        }

        var res = string.Join("", stack.ToArray().Reverse()).TrimStart('0');

        return res.Length == 0 ? "0" : res;
    }
}

public class Solution2
{
    public string RemoveKdigits(string num, int k)
    {
        //corner case
        if (k >= num.Length)
            return "0";


        var st = new Stack<int>();

        //its a straigh fwd case of stack
        //we will from from left, left digit will most signifacnt, it will contribute in max number gen
        //so we will remove number from left

        for (int left = 0; left < num.Length; left++)
        {
            int n = num[left] - '0';
            //Console.WriteLine($"n: {n}");
            while (k > 0 && st.Count > 0 && st.Peek() > n)
            {
                st.Pop();
                k--;
            }

            st.Push(n);
        }

        //say number is strictly increasing then k wont be uitlized
        //like "3456", k = 2;
        while (k > 0)
        {
            st.Pop();
            k--;
        }

        //front zero does not make any sense, trim that
        var result = string.Join("", st.ToArray().Reverse()).TrimStart('0');

        //if length is zero,means our result would be "00" or "0" so in this case we have to return zero
        return result.Length == 0 ? "0" : result;
    }
}