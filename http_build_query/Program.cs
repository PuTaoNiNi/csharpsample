﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace http_build_query
{
    class Program
    {
        static void Main(string[] args)
        {
            var introduction = new
            {
                amount = 300.00,
                bank_account_info = new
                {
                    BANK = "ICBKCNBJ",
                    BANK_BRANCH = "Branch",
                    CARD_HOLDER_NAME = "葡 萄",
                    CARD_NUMBER = "666666666666666666",
                    CITY = "City",
                    PROVINCE = "Province",
                },
                test = new List<string>
                {
                    "test1",
                    "test2"
                },
                merchant_number = "20211221145543903233",
                notify_url = "http://example.com",
                order_number = DateTime.Now,
                created_at = "2021-12-24 20:00:37",
                updated_at = "2021-12-24 20:18:45",
            };


            var test = GetTest(introduction);

            Console.WriteLine(test);
        }

        private static string GetTest(object introduction, string parentKey = "")
        {
            var type = introduction.GetType();

            var test = string.Join("&", type.GetProperties().OrderBy(kvp => kvp.Name).Select(kvp =>
            {
                var obj = string.Empty;
                var test = kvp.GetValue(introduction);

                var kvpType = (kvp.PropertyType == typeof(string) || kvp.PropertyType.IsValueType) ? null : kvp.PropertyType.GetProperties();

                if (kvpType != null)
                    Console.WriteLine($"{kvp.Name} : {string.Join(":", kvpType.Select(x => x.Name))} {kvpType.Length}");
                switch (test)
                {
                    case bool boolObj:
                        obj = boolObj ? "1" : "0";
                        break;

                    case DateTime datetime:
                        obj = datetime.ToString("yyyy-MM-dd HH:mm:ss");
                        break;

                    case IDictionary dictionary:
                        var key2 = Uri.EscapeDataString(kvp.Name);
                        obj = string.Join($"&{key2}", GetDictionaryAdapter(dictionary));

                        return $"{key2}{Uri.EscapeDataString(obj)}";

                    case ICollection collection:
                        var key1 = Uri.EscapeDataString(kvp.Name);
                        obj = string.Join($"&{key1}", GetArrayAdapter(collection));

                        return $"{key1}{Uri.EscapeDataString(obj)}";

                    case object _ when kvpType is not null:
                        return GetTest(test, kvp.Name);

                    default:
                        obj = kvp.GetValue(introduction).ToString();
                        break;
                }
                var key = string.IsNullOrEmpty(parentKey) ? Uri.EscapeDataString(kvp.Name) : Uri.EscapeDataString($"{parentKey}[{kvp.Name}]");
                var value = Uri.EscapeDataString(obj).Replace("%20", "+");

                return $"{key}={value}";
            }));

            return test;
        }

        private static IEnumerable<string> GetArrayAdapter(ICollection collection)
        {
            var i = 0;

            foreach (var item in collection)
            {
                yield return $"[{i.ToString()}]={item}";

                i++;
            }
        }

        private static IEnumerable<string> GetDictionaryAdapter(IDictionary collection)
        {
            foreach (DictionaryEntry item in collection)
            {
                yield return $"[{item.Key.ToString()}]={item.Value}";
            }
        }
    }
}
