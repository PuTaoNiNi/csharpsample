﻿// See https://aka.ms/new-console-template for more information
var pokemon = new Squirtle();
Console.WriteLine(pokemon.GetAttack());

public interface IPokemon
{
    public string GetAttack();
}

public class Squirtle : IPokemon
{
    protected readonly string Name = "傑尼龜";

    private readonly string SkillName = "水槍";

    public string GetAttack()
    {
        return $"{Name} 使用 {SkillName} 攻擊";
    }
}
