﻿// See https://aka.ms/new-console-template for more information
var pokemon = new Pokemon();
pokemon.Name = "皮卡丘";
pokemon.Property = "電";
pokemon.SkillName = "十萬伏特";

Console.WriteLine(pokemon.GetProperty());
Console.WriteLine(pokemon.GetAttack());


public class Pokemon
{
    public string Name { get; set; }

    public string Property { get; set; }

    public string SkillName { get; set; }

    public string GetProperty()
    {
        return $"{Name} 屬性: {Property}";
    }

    public string GetAttack()
    {
        return $"{Name} 使用 {SkillName} 攻擊";
    }
}