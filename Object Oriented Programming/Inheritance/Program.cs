﻿// See https://aka.ms/new-console-template for more information
var pokemon1 = new Bulbasaur();
Console.WriteLine(pokemon1.GetAttack());

var pokemon2 = new Ivysaur();
Console.WriteLine(pokemon2.GetAttack());
Console.WriteLine(pokemon2.GetAttack1());

public interface IPokemon
{
    public string GetAttack();
}

public class Bulbasaur : IPokemon
{
    protected string Name = "妙蛙種子";

    private readonly string SkillName = "藤鞭";

    public string GetAttack()
    {
        return $"{Name} 使用 {SkillName} 攻擊";
    }
}

public class Ivysaur : Bulbasaur
{
    private readonly string SkillName1 = "飛葉快刀";

    public Ivysaur()
    {
        base.Name = "妙蛙草";
    }

    public string GetAttack1()
    {
        return $"{Name} 使用 {SkillName1} 攻擊";
    }
}