﻿// See https://aka.ms/new-console-template for more information
var pokemon1 = new Bulbasaur();
Console.WriteLine(pokemon1.GetAttack());

var pokemons = new List<IPokemon>();

IPokemon pokemon;

pokemon = new Charizard();
Console.WriteLine(pokemon.GetAttack());

pokemon = new CharizardX();
Console.WriteLine(pokemon.GetAttack());
Console.WriteLine(((CharizardX)pokemon).GetAttack1());

public interface IPokemon
{
    public string GetAttack();
}

public class Bulbasaur : IPokemon
{
    protected string Name = "喵喵";

    private readonly string SkillName = "聚寶功";

    private readonly decimal Money = 100m;

    public string GetAttack()
    {
        return $"{Name} 使用 {SkillName} 攻擊，獲得 {Money} 元";
    }
}


public class Pokemon
{
    public string GetName()
    {
        return "喵喵";
    }

    public string GetName(string name)
    {
        return name;
    }
}

public class Charizard : IPokemon
{
    protected string Name = "噴火龍";

    private readonly string SkillName = "噴射火焰";

    public string GetAttack()
    {
        return $"{Name} 使用 {SkillName} 攻擊";
    }
}

public class CharizardX : Charizard
{
    public CharizardX()
    {
        base.Name = "超級噴火龍Ｘ";
    }

    private readonly string SkillName1 = "龍爪";

    public string GetAttack1()
    {
        return $"{Name} 使用 {SkillName1} 攻擊";
    }
}