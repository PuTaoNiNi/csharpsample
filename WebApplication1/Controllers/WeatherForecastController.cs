﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace WebApplication1.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly ILogger<WeatherForecastController> _logger;

        public WeatherForecastController(ILogger<WeatherForecastController> logger)
        {
            _logger = logger;
        }

        static void Log(int num, string guid, string msg)
        {
            Console.WriteLine("({0}) T{1}: {2} {3}",
                num, Thread.CurrentThread.ManagedThreadId, msg, guid);
        }

        static async Task<string> MyDownloadPageAsync(string guid, string url)
        {
            Log(2, guid, "正要呼叫 WebClient.DownloadStringTaskAsync()。");

            using (var webClient = new WebClient())
            {
                var task = webClient.DownloadStringTaskAsync(url);

                Log(3, guid, "已起始非同步工作 DownloadStringTaskAsync()。");

                string content = await task;

                Log(5, guid, "已經取得 DownloadStringTaskAsync() 的結果。");

                return content;
            }
        }

        [HttpGet]
        public async Task<IEnumerable<WeatherForecast>> GetAsync()
        {
            ThreadPool.SetMinThreads(1, 1);
            using (SemaphoreSlim concurrencySemaphore = new SemaphoreSlim(1, 1))
            {
                concurrencySemaphore.Wait();

                Log(1, HttpContext.TraceIdentifier, "正要起始非同步工作 MyDownloadPageAsync()。");

                var task = MyDownloadPageAsync(HttpContext.TraceIdentifier, "https://www.huanlintalk.com");

                Log(4, HttpContext.TraceIdentifier, "已從 MyDownloadPageAsync() 返回，但尚未取得工作結果。");


                string content = await task;

                Log(6, HttpContext.TraceIdentifier, "已經取得 MyDownloadPageAsync() 的結果。");

                Console.WriteLine("網頁內容總共為 {0} 個字元。", content.Length);

                concurrencySemaphore.Release();
            }

            var rng = new Random();
            return Enumerable.Range(1, 5).Select(index => new WeatherForecast
            {
                Date = DateTime.Now.AddDays(index),
                TemperatureC = rng.Next(-20, 55),
                Summary = Summaries[rng.Next(Summaries.Length)]
            })
            .ToArray();
        }
    }
}
