﻿// See https://aka.ms/new-console-template for more information
using System.Collections;

var arrayList = new ArrayList();
arrayList.Add(1);
arrayList.Add("2");

var intList = new List<int>();
intList.Add(1);
// intList.Add("2"); // Compiler Error CS1503

public class Animal
{
    public int Name { get; set; }
}

public class Dog : Animal
{

}

public class Cat : Animal
{

}

public class AnimalHouse<T>
{
    public List<T> House { get; set; }
}

public class DogHouse
{
    public List<Dog> House { get; set; }
}

public class CatHouse
{
    public List<Cat> House { get; set; }
}