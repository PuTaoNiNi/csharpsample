﻿// See https://aka.ms/new-console-template for more information

// generic type
var generic = new Generic<string>() { Field = "Hello Generic" };
Console.WriteLine(generic.Field);

// generic type definition
var d1 = typeof(Dictionary<,>);

var isGeneric = d1.IsGenericType;
var isDefinition = d1.IsGenericTypeDefinition;

Console.WriteLine($"Is this a generic type? {isGeneric}");
// True 
Console.WriteLine($"Is this a generic type definition? {isDefinition}");
// True

// constructed generic type
Type d2 = typeof(Dictionary<int, string>);

Console.WriteLine("   Is this a generic type? {0}",
    d2.IsGenericType);
Console.WriteLine("   Is this a generic type definition? {0}",
    d2.IsGenericTypeDefinition);

// generic type parameter
Type[] typeParameters1 = d1.GetGenericArguments();
var tKey = typeParameters1[0].Name;
var tValue = typeParameters1[1].Name;
Console.WriteLine($"TKey:{tKey}, TValue:{tValue}");

// generic type argument
Type[] typeParameters2 = d2.GetGenericArguments();
tKey = typeParameters2[0].Name;
tValue = typeParameters2[1].Name;
Console.WriteLine($"TKey:{tKey}, TValue:{tValue}");

// Covariance
List<string> stringList1 = new List<string>();
IEnumerable<object> objects1 = stringList1;

// Contravariance
Action<object> b = (target) => { Console.WriteLine(target.GetType().Name); };
Action<string> d = b;
d(string.Empty);

// generic type
//var constraintGeneric = new ConstraintGeneric<string>() { Field = "Hello Generic" };
//Console.WriteLine(generic.Field);


public class Generic<T>
{
    public T Field;
}

// Constraint
public class ConstraintGeneric<T> where T : new()
{
    public T Field;
}