﻿// See https://aka.ms/new-console-template for more information
using System.Collections;
using System.Diagnostics;

var count = 100000;
var stopwatch = new Stopwatch();

stopwatch.Restart();
stopwatch.Start();

var arrayList = new ArrayList();
var result = 0;
for (int i = 0; i < count; i++)
{
    arrayList.Add(i);
}
foreach (int item in arrayList)
{
    result += item;
}

Console.WriteLine($"Sum : {result}");

stopwatch.Stop();
Console.WriteLine($"Total Milli Seconds : {stopwatch.Elapsed.TotalMilliseconds}");

stopwatch.Restart();
stopwatch.Start();

var intList = new List<int>();
result = 0;
for (int i = 0; i < count; i++)
{
    intList.Add(i);
}
foreach (int item in intList)
{
    result += item;
}

Console.WriteLine($"Sum : {result}");

stopwatch.Stop();
Console.WriteLine($"Total Milli Seconds : {stopwatch.Elapsed.TotalMilliseconds}");