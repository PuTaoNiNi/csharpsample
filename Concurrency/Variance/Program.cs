﻿using System.Collections.Generic;

namespace Variance
{
    class Program
    {
        public class Color
        {
            public string Name { get; set; }

            public int Red { get; set; }

            public int Green { get; set; }

            public int Blue { get; set; }
        }

        public interface IPaintable
        {
            Color Color { get; set; }
        }

        public class Car : IPaintable
        {
            public Color Color { get; set; }
        }

        public interface IBrush<T> where T : IPaintable
        {
            void Paint(T objectToPaint, Color color);
        }
        public class Brush<T> : IBrush<T> where T : IPaintable
        {
            public void Paint(T objectToPaint, Color color)
            {
                objectToPaint.Color = color;
            }
        }

        static void Main(string[] args)
        {
            IBrush<IPaintable> brush = new Brush<IPaintable>();

            IBrush<Car> carBrush = (IBrush<Car>)brush;

            carBrush.Paint(new(), new Color());

            IEnumerable<IPaintable> test = new List<Car>();
        }
    }
}
