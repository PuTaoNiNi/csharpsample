﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Volatile
{
    class Program
    {
        static async Task Main(string[] args)
        {
            var worker = new Worker();

            Console.WriteLine("主執行緒：啟動工作執行緒...");
            var workerTask = Task.Run(worker.DoWork);

            // 等待 500 毫秒以確保工作執行緒已在執行
            Thread.Sleep(500);

            Console.WriteLine("主執行緒：請求終止工作執行緒...");
            worker.RequestStop();

            // 待待工作執行緒執行結束
            await workerTask;

            Console.WriteLine("主執行緒：工作執行緒已終止");
        }
    }

    public class Worker
    {
        //private volatile bool _shouldStop;
        private bool _shouldStop;

        public void DoWork()
        {
            bool work = false;

            while (!_shouldStop)
            {
                work = !work;
            }

            Console.WriteLine("工作執行緒：正在終止...");
        }

        public void RequestStop()
        {
            _shouldStop = true;
        }
    }
}
