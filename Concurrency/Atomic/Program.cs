﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Atomic
{
    class Program
    {
        static object _lock = new object();

        static async Task Main(string[] args)
        {
            var accountA = new BankAccount();
            accountA.Balance = 10000;
            var accountB = new BankAccount();

            // Ex 1 :
            var tasks = new List<Task>();

            for (var i = 0; i < 100; i++)
            {
                tasks.Add(Task.Run(() =>
                {
                    lock (_lock)
                    {
                        accountA.Balance -= 100;
                        accountB.Balance += 100;
                    }
                }));
            }

            await Task.WhenAll(tasks);

            Console.WriteLine($"Account A remaining balance is {accountA.Balance}");
            Console.WriteLine($"Account B remaining balance is {accountB.Balance}");
        }
    }

    public class BankAccount
    {
        public decimal Balance { get; set; }
    }
}
