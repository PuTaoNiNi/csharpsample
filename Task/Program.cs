﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace TaskSample
{
    class Program
    {
        static async Task Main(string[] args)
        {
            CancellationTokenSource src = new CancellationTokenSource();
            CancellationToken ct = src.Token;
            ct.Register(() => Console.WriteLine("Abbruch des Tasks"));

            // t <= taskA 1000
            Task t = Task.Run(() =>
            {
                System.Threading.Thread.Sleep(5000);
                //ct.ThrowIfCancellationRequested();
            }, ct);


            try
            {
                //src.CancelAfter(300);
                t.Wait(300);
            }
            catch (Exception e)
            {
                // Don't actually use an empty catch clause, this is
                // for the sake of demonstration.
                Console.WriteLine(e.Message);
            }

            Console.WriteLine("Canceled: {0} . Finished: {1} . Error: {2}",
                               t.IsCanceled, t.IsCompleted, t.IsFaulted);
        }
    }
}
