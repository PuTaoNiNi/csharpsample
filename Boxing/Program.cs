﻿using System;

namespace Boxing
{
    class Program
    {
        static void Main(string[] args)
        {
            // ex 1
            var ex1 = 123;

            Console.WriteLine($" {ex1}");             // Boxing
            Console.WriteLine($" {ex1.ToString()}");

            // ex 2
            var ex2 = "123";

            object obj2 = ex2;

            obj2 = "456";

            Console.WriteLine($"Example2 string result: {ex2}");
            Console.WriteLine($"Example2 object result: {obj2}");

            // ex 3
            var cls3_1 = new ExampleClass();

            var cls3_2 = cls3_1;

            cls3_2.MyProperty = "456";

            Console.WriteLine($"Example3 class 1 result: {cls3_1.MyProperty}");
            Console.WriteLine($"Example3 class 2 result: {cls3_1.MyProperty}");

            // ex 4
            var cls4_1 = new ExampleClass();

            cls4_1.MyProperty = "123";

            var cls4_2 = cls3_1;

            cls4_2 = new ExampleClass();

            cls4_2.MyProperty = "456";

            Console.WriteLine($"Example4 class 1 result: {cls4_1.MyProperty}");
            Console.WriteLine($"Example4 class 2 result: {cls4_2.MyProperty}");
        }

        public class ExampleClass
        {
            public string MyProperty { get; set; }
        }
    }
}
