﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DesignLibrary
{
    public class IntegerCollections
    {
        public void NotNestedCollection(ICollection<int> collection)
        {
            foreach (int i in collection)
            {
                Console.WriteLine(i);
            }
        }

        // This method violates the rule.
        public void NestedCollection(
           ICollection<ICollection<int>> outerCollection)
        {
            foreach (ICollection<int> innerCollection in outerCollection)
            {
                foreach (int i in innerCollection)
                {
                    Console.WriteLine(i);
                }
            }
        }
    }

    public class Outermost<T>
    {
        public class Inner<U>
        {
            public class Innermost1<V> { }
            public class Innermost2 { }
        }
    }

    public class ParentType
    {
        public class NestedType
        {
            public NestedType()
            {
            }
        }

        public ParentType()
        {
            NestedType nt = new NestedType();
        }
    }

    class Test
    {
        static void Main()
        {
            List<int> testDatas = new List<int> { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

            int condition = 1;

            var selectDatas = testDatas.Where(i => i > condition);

            foreach (var oneData in selectDatas)
            {
                condition += 2;
                Console.WriteLine($"{oneData}");
            }
        }

        public static IEnumerable<char> Test1()
        {
            IEnumerable<char> query = "How are you, friend.";

            foreach (char vowel in "aeiou")
                query = query.Where(c => c != vowel);


            return query.Where(s => s == ' ');
        }
    }
}